# File for module specific CMake tests.

find_package(muParser)
 if (muParser_FOUND)
  dune_register_package_flags(COMPILE_DEFINITIONS HAVE_MUPARSER ${MUPARSER_DEFINITIONS}
                              LIBRARIES ${MUPARSER_LIBRARIES}
                              INCLUDE_DIRS ${MUPARSER_INCLUDE_DIRS})
endif()
